Ensemble des accées par type d'utilisateurs et type de données
---------------------------------------------------------------
// ces roles correspendent respectivement à ()
nos roles ['medCons','medOp','medRad','medLab','admin']

 Consultation

  GetAll             admin
  AddOne             MedCons
! DeleteOne          admin
  GetOne             admin
# Getidpdate         MedCons
! UpdateOne          admin
# getallssn          MedCons/MedOp
# getallself         Patient
  

 Intervention

  GetAll             admin
  AddOne             medOp
! DeleteOne          medOp
  GetOne             admin
# Getidpdate         medOp
! UpdateOne          medOp
# getallssn          medRad/medOp/medCons
# getallself         patient

 Analyse

  GetAll             admin
  AddOne             medLab
! DeleteOne          admin
  GetOne             admin
# Getbyssndate       medLAb
! UpdateOne          admin
# getallssn          medLab/medCons/medOp
# getallself         patient
 

Radiographie

  GetAll             admin
  AddOne             medRad
! DeleteOne          admin
  GetOne             admin
# Getbyssndate       medRad
! UpdateOne          admin
# getallssn          medCons/medOp
# getallself         patient



 Patient

  GetAll             admin
  AddOne             none
  DeleteOne          admin
  GetOnebyid         admin
  UpdateOne          admin
# Updateself         patient
# getself            patient

 Agent

  GetAll             admin
  AddOne             none
  DeleteOne          admin
  GetOnebyid         admin
  UpdateOne          admin
# Updateself         Agent
# getself            Agent