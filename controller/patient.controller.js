const Patient = require("../models/patient.model");
const passport = require('passport');
const jwt = require('jsonwebtoken');
const access = require("../middlewares/authorization");
const fs = require("fs");


module.exports = {

    // login function of a patient
    login: (req, res, next) => {
        passport.authenticate('login-patient',
            (err, user, info) => {
                if (err || !user){
                    res.render('login-patient',{error:info.message});
                }
                else{
                    req.login(user, { session: false },
                        (error) => {
                            if (error) return next(error)
                            // generating jwt
                            const body = { 
                                _id: user._id,
                                ssn:user.ssn,
                                email: user.email, 
                                role: "patient" 
                            };
                            const token = jwt.sign({ user: body }, 'top_secret');
                            fs.writeFile("test-jwtoken.txt", token, (err) => {
                                if (err) console.log(err);
                                console.log("jwt generated.");
                            });
                            res.render('home-patient',{jwtoken:token});
                        }
                    );
                }
            }
        )(req, res, next);
    },
    // get all patient for admin only
    getAll: (req, res, next) => {

        access.authorize(req,["admin"])

        Patient.find({}).then((patients) => {
            res.statusCode = 200;
            res.setHeader("content-Type", "application/json");
            res.json(patients);

        }, err => next(err)
        )
            .catch(err => next(err));
    },

    // add one new patient
    addOne: (req, res, next) => {

        access.authorize(req,["admin"])

        passport.authenticate('signup-patient',
            (err, user, info) => {
                if (err || !user) return res.json(info);
                Patient.create(req.body)
                    .then((patient) => {
                        res.statusCode = 200;
                        res.setHeader("content-Type", "application/json");
                        res.json(patient);
                    }, err => next(err))
                    .catch(err => next(err)
                    );
            }
        )(req, res, next);
    },

    // delete one patient based on id admin only
    deleteOne: (req, res, next) => {

        access.authorize(req,["admin"])

        Patient.findByIdAndDelete(req.params.NSS)
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(resp)

            }, err => next(err)
            )
            .catch(err => next(err));

    },

    // get on specific patient based on id admin only
    getOne: (req, res, next) => {

        access.authorize(req,["admin"])

        Patient.find({ssn:req.params.ssn})
            .then((patient) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(patient);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

    // update a patient info
    updateOne: (req, res, next) => {

        access.authorize(req,["patient"])

        // le paramettre new force la fonction a renvoyer la nouvelle version de patient
        Patient.findOneAndUpdate({ssn:req.user.ssn}, { $set: req.body }, { new: true })
            .then((patient) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(patient);

            }, err => next(err)
            )
            .catch(err => next(err));

    },
    
    // get the patient info
    getSelf:(req, res, next) => {
       
        access.authorize(req,["patient"])

        Patient.find({ssn:req.user.ssn})
            .then((patient) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(patient);

            }, err => next(err)
            )
            .catch(err => next(err));

    },

}