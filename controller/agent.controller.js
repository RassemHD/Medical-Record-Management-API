const Agent = require("../models/agent.model");
const passport = require('passport');
const jwt = require('jsonwebtoken');
const access = require("../middlewares/authorization");
const fs = require("fs");


module.exports = {

    //login to an existing account
    login: (req, res, next) => {
        passport.authenticate('login-agent',
            (err, user, info) => {
                if (err || !user){
                    res.render('login-agent',{error:info.message});
                }
                else{
                    req.login(user, { session: false },
                        (error) => {
                            if (error) return next(error)
                            // generating jwt
                            const body = {
                                _id: user._id, 
                                ssn:user.ssn,
                                email: user.email, 
                                role: user.accountType 
                            };
                            const token = jwt.sign({ user: body }, 'top_secret');

                            fs.writeFile("test-jwtoken.txt", token, (err) => {
                                if (err) console.log(err);
                                console.log("jwt generated.");
                            });
                            res.render('home-'+user.accountType.toLowerCase(),{jwtoken:token});
                        }
                    );
                }
            }
        )(req, res, next);
    },

    //get all agents
    getAll: (req, res, next) => {

        access.authorize(req,["admin"])

        Agent.find({}).then((agents) => {
            res.statusCode = 200;
            res.setHeader("content-Type", "application/json");
            res.json(agents);

        }, err => next(err)
        )
            .catch(err => next(err));
    },

    // add one agent 
    addOne: (req, res, next) => {

        access.authorize(req,["admin"])

        passport.authenticate('signup-agent',
            (err, user, info) => {
                if (err || !user) return res.json(info);
                Agent.create(req.body)
                    .then((agent) => {
                        res.statusCode = 200;
                        res.setHeader("content-Type", "application/json");
                        res.json(agent);
                    }, err => next(err))
                    .catch(err => next(err)
                    );
            }
        )(req, res, next);
    },

    // delete an agent
    deleteOne: (req, res, next) => {

        access.authorize(req,["admin"])
        
        Agent.findOnedAndDelete({ssn:req.params.ssn})
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(resp)

            }, err => next(err)
            )
            .catch(err => next(err));

    },

    // retrive one agent
    getOne: (req, res, next) => {

        access.authorize(req,["admin"])

        Agent.find({ssn : req.params.ssn})
            .then((agent) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(agent);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

    //update the informations relted to one agent
    updateOne: (req, res, next) => {

        access.authorize(req,["admin"])

        // getting the new updated values as a response
        Agent.findOneAndUpdate({ssn:req.params.ssn}, { $set: req.body }, { new: true })
            .then((agent) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(agent);

            }, err => next(err)
            )
            .catch(err => next(err));

    },

    // get the info about an agent account
    getSelf:(req, res, next) => {
       
        access.authorize(req,["medOp","medCons","medRad","medLab"])

        Agent.find({ssn:req.user.ssn})
            .then((patient) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(patient);

            }, err => next(err)
            )
            .catch(err => next(err));

    },

}