const mongoose = require("mongoose");
const Analyse = require("../models/analyse.model");
const access = require("../middlewares/authorization");



module.exports = {

    // get all the analysis objects
    getAll: (req, res, next) => {

        access.authorize(req,["admin"])

        Analyse.find({}).then((analyses) => {
            res.statusCode = 200;
            res.setHeader("content-Type", "application/json");
            res.json(analyses);

        }, err => next(err)
        )
            .catch(err => next(err));
    },


    // get all the patient of a given connected doctor
    getAllByDoctor:(req, res, next) => {

        access.authorize(req,["admin","medLab"])

        var ssn = req.user.ssn
        if(req.user.role == "admin") ssn = req.body.ssnAgent
        
        Analyse.find({ssnAgent:ssn})
            .then((analyses) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(analyses);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

    //get all the analysis of a single connected patient
    getAllByPatient:(req, res, next) => {

        access.authorize(req,["patient"])
        
        Analyse.find({ssnPatient:req.user.ssn})
            .then((analyses) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(analyses);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

    //get all the analysis of a single patient from its ssn
    // by a connected doctor
    getAllByPatientForDoctor:(req, res, next) => {

        access.authorize(req,["medLab"])
        
        Analyse.find({ssnAgent:req.user.ssn,ssnPatient:req.params.ssnp})
            .then((analyses) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(analyses);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

    // get all the analysis of a certain type made by a connected doctor
    getAllByTypeForDoctor:(req, res, next) => {

        access.authorize(req,["medLab"])
        
        Analyse.find({ssnAgent:req.user.ssn,analysisType:req.params.type})
            .then((analyses) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(analyses);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

    // get all the analysis of a certain type of a connected patient
    getAllByTypeForPatient:(req, res, next) => {

        access.authorize(req,["patient"])
        
        Analyse.find({ssnPatient:req.user.ssn,analysisType:req.params.type})
            .then((analyses) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(analyses);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

    // get all the analysis of a certain type of a given patient for a connected doctor
    getAllByTypeAndPatientForDoctor:(req, res, next) => {

        access.authorize(req,["medCons","medOp"])
        
        Analyse.find({
                ssnPatient:req.params.ssnp,
                analysisType:req.params.type
            })
            .then((analyses) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(analyses);

            }, err => next(err)
            )
            .catch(err => next(err));
    },


    // add one new analysis
    addOne: (req, res, next) => {

        access.authorize(req,["medLab"])

        const analyse = new Analyse(
            {
                _id: new mongoose.Types.ObjectId(),
                ssnPatient: req.body.ssnPatient,
                ssnAgent: req.user.ssn,
                date: req.body.date,
                title: req.body.title,
                analysisType: req.body.analysisType,
                path: req.file.path.replace("\\", "/")

            });
        analyse.save()
            .then((analyse) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(analyse);
                // gestion des erreures via la focntion next
            }, err => next(err)
            )
            .catch(err => next(err));

    },

    //delete one analysis by id
    deleteOne: (req, res, next) => {

        access.authorize(req,["admin"])

        Analyse.findByIdAndDelete(req.params.id)
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(resp)

            }, err => next(err)
            )
            .catch(err => next(err));

    },

    // get one analysis by id
    getOne: (req, res, next) => {

        access.authorize(req,["admin"])

        Analyse.findById(req.params.id)
            .then((analyse) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(analyse);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

    // get all the analysis of one patient
    getOnePatientForDoctor:(req, res, next) => {

        access.authorize(req,["medCons","medOp"])
        
        Analyse.find({ssnPatient:req.params.ssnp})
            .then((analyses) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(analyses);

            }, err => next(err)
            )
            .catch(err => next(err));

    },

    // delete one analysis made by the connected agent
    deleteOneSelf:(req, res, next) => {

        access.authorize(req,["medLab"])

        Analyse.findOneAndDelete({_id:req.params.id,ssnAgent:req.user.ssn})
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(resp)

            }, err => next(err)
            )
            .catch(err => next(err));

    },

    // update one analysis made by the agent
    updateOne: (req, res, next) => {

        access.authorize(req,["medLab"])

        Analyse.findOneAndUpdate({_id:req.params.id,ssnAgent:req.user.ssn}, { 
           
                ssnPatient: req.body.ssnPatient,
                ssnAgent: req.user.ssn,
                date: req.body.date,
                title: req.body.title,
                analysisType: req.body.analysisType,
                path: req.file.path.replace("\\", "/")

        }, { new: true })
            .then((analyse) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(analyse);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

}