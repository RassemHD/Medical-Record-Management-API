const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const JWTstrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;

const Patient = require('../models/patient.model');
const Agent = require('../models/agent.model');

var userModel;

async function verifyCredentials (email, password, done)
{
    try {
        const user = await userModel.findOne({ email });
        if(!user){
            return done(null, false, { message : 'User not found'});
        }
        const validate = await user.isValidPassword(password);
        if( !validate ){
            return done(null, false, { message : 'Wrong Password'});
        }
        //Send the user information to the next middleware
        return done(null, user, { message : 'Logged in Successfully'});
    }catch (error){
        return done(error);
    }	
}

async function register(email, password, done)
{
    try {        
        const user = await userModel.findOne({ email });
        if(user){
            return done(null, false, { message : 'Email alredy exists!'});
        }

        if(password.length < 6){
            return done(null, false, { message : 'Password too short!'});
        }
        return done(null,true, { message : 'Email accepted!'});

    } catch (error) {
      done(error);
    }
}

// Login middlewares

passport.use('login-patient', new localStrategy({
    usernameField : 'email',
    passwordField : 'password'
},
(email, password, done) => {
    userModel = Patient;
    verifyCredentials(email, password, done);
}));

passport.use('login-agent', new localStrategy({
    usernameField : 'email',
    passwordField : 'password'
},
(email, password, done) => {
    userModel = Agent;
    verifyCredentials(email, password, done);
}));

// Signup middlewares

passport.use('signup-patient', new localStrategy({
    usernameField : 'email',
    passwordField : 'password'
},
(email, password, done) => {
    userModel = Patient;
    register(email, password, done);
}));

passport.use('signup-agent', new localStrategy({
    usernameField : 'email',
    passwordField : 'password'
},
(email, password, done) => {
    userModel = Agent;
    register(email, password, done);
}));

//This verifies that the token sent by the user is valid
passport.use(new JWTstrategy({
    //secret we used to sign our JWT
    secretOrKey : 'top_secret',            
    //we expect the user to send the token as a query paramater with the name 'secret_token'
    jwtFromRequest : ExtractJWT.fromHeader('jwt')
},
async (token, done) => {
    try {
      //Pass the user details to the next middleware
      return done(null, token.user);
    } catch (error) {
      done(error);
    }
}));