#Authors:
    Rassem HACHOUD
    M'hamed TAYEB


M1 MIAGE Paris Descarte
Web Programming

#title

  "Medical file managment"
  

Application web pour la Gestion Informatisée des Dossiers Médicaux des Patients GIDMP

- Utilisateurs de l'application:

	* Medecins (Consultants, Opérants, Laborentins, Radiologues)
	* Patients
	* Administrateur

- Fragements de dossier à manipuler:

 	* Résultats d'analyses médicales d'un patient (PDF)
 	* Clichées de radiologie (RX, scanners et echographies)
 	* Comptes rendus de consultation
 	* Comptes rendus d'interventions subies par le patient
	* Récapitulatif de l'ensemble des hospitalisation

- Fonctionnalités:
	
	* Administrateur :
		- Ajout / cloture des comptes d'utilisateurs

	* Patients :
		- Inscriptions																						
		- Visualisation de dossier médicale personnel	

	* Médecins consultants :													
   		- Ajout / suppression de ses propres comptes rendus de consultation		
   		- consultation en lecture seule de l'ensemble des dossier médicaux des patients

	* Médecins opérants :
		- Ajout / suppression de ses propres comptes rendus d'intervention / hospitalisations
   		- consultation en lecture seule de l'ensemble des dossier médicaux des patients 

	* Médecins laborentins :
		- Ajout / suppression de ses propres comptes rendus d'analyses médicales
   		- consultation en lecture seule de l'ensemble des comptes rendus d'analyse médicale

	* Radiologues :				
		- Ajout / suppression de ses propres radios
   		- consultation en lecture seule de l'ensemble des radios

  