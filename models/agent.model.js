const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const schema = mongoose.Schema;

const agentSchema = new schema(
    {
        firstName: { type: String, required: true ,match:[,'caractères non supporté']},
        lastName: { type: String, required: true },
        birthDate: { type: Date, required: true },
        gender: { type: String, enum: ['M', 'F'], required: true },
        address: { type: String, required: true },
        ssn: { type: String, required: true },
        email: { type: String, required: true ,match:[/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,'email non valide']},
        password: { type: String, required: true },
        accountType: { type: String, enum:['medCons','medOp','medRad','medLab','admin'], required: true }
    },
    // ajoute date de création et date de Maj
    { collection: "agent", timestamps: true }
);


// this function hashes the password, before saving the user in the database
agentSchema.pre('save', async function (next) {
    //Hash the password with a salt round of 10
    const hash = await bcrypt.hash(this.password, 10);
    this.password = hash;
    next();
});

// verify password
agentSchema.methods.isValidPassword = async function (password) {
    //Hashes the password sent by the user for login and checks if the hashed password stored in the database matches the one sent. Returns true if it does else false.
    const compare = await bcrypt.compare(password, this.password);
    return compare;
}

module.exports = mongoose.model("agent", agentSchema);