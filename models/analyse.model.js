const mongoose = require('mongoose');
const schema = mongoose.Schema;

const analyseSchema = new schema(
   {
      ssnPatient: { type: String, required: true },
      ssnAgent: { type: String, required: true },
      date: { type: Date, required: true },
      title: { type: String, required: false },
      analysisType: { type: String, enum: ['blood', 'stools', 'urine'], required: true },
      path: { type: String, required: true },

   },
   // ajoute date de création et date de Maj
   { collection: "analyse", timestamps: true }

);

module.exports = mongoose.model("Analyse", analyseSchema);