const mongoose = require("mongoose");

// liens de base de données
const url = "mongodb://localhost:27017/Medic";
const connect = mongoose.connect(url);

connect.then(db => {
    console.log(`Connected correctly to the server: ${url}`);
});

module.exports = mongoose.connection