const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

const schema = mongoose.Schema;

const patientSchema = new schema(
    {
        firstName: { type: String, required: true },
        lastName: { type: String, required: true },
        birthDate: { type: Date, required: true },
        gender: { type: String, enum: ['M', 'F'], required: true },
        address: { type: String, required: true },
        bloodGroup: { type: String, enum: ['A+', 'B+', 'AB+', 'O+', 'A-', 'B-', 'AB-', 'O-'] },
        ssn: { type: String, required: true },
        email: { type: String, required: true },
        password: { type: String, required: true }
    },
    {
        collection: "patient",
        timestamps: true
    }
);

// this function hashes the password, before saving the user in the database
patientSchema.pre('save', async function (next) {
    //Hash the password with a salt round of 10
    const hash = await bcrypt.hash(this.password, 10);
    this.password = hash;
    next();
});

// verify password
patientSchema.methods.isValidPassword = async function (password) {
    //Hashes the password sent by the user for login and checks if the hashed password stored in the database matches the one sent. Returns true if it does else false.
    const compare = await bcrypt.compare(password, this.password);
    return compare;
}

module.exports = mongoose.model("Patient", patientSchema);